<?php
    $mensaje="";
    if(isset($_POST["envio"])){
        include("php/envioCorreo.php");
        $email = new email("JosTecHQ","josluna1098@gmail.com","xqvhiaewamligfml");
        $email->agregar($_POST["email"],$_POST["nombre"]);
        if ($email->enviar('Prueba envio de correos',$contenido_html)){
                $mensaje= 'Mensaje enviado';
        }else{
                $mensaje= 'El mensaje no se pudo enviar';
            $email->ErrorInfo;
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5Z2BMVK');</script>
        <!-- End Google Tag Manager -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128060864-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-128060864-1');
        </script>
        <!-- End Google Analytics -->
        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link rel="stylesheet" href="css/custom.css">
        
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <title>JosTecHQ</title> -->

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:700%7CNunito:300,600" rel="stylesheet"> 

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
                
        <!-- my css -->
        <link type="text/css" rel="stylesheet" href="css/post.css"/>

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css"/>
    </head>
    
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5Z2BMVK"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

	<!-- Header -->
	<header id="header">
            <!-- Nav -->
            <div id="nav">
		<!-- Main Nav -->
		<div> <!-- id="nav-fixed" -->
                    <div class="container">
                        <!-- logo -->
                        <div class="nav-logo">
                            <a href="index.php" class="logo"><h1>JosTecHQ</h1></a>
                        </div>
                        <!-- /logo -->

                        <!-- nav -->
			<ul class="nav-menu nav navbar-nav">
                            <li><a href="index.php">Inicio</a></li>
                            <li><a href="#" class="about">Sobre mi</a></li>
                            <li><a href="correo/index.php" class="contact">Contacto</a></li>
                        </ul>
			<!-- /nav -->
                                                
			<!-- aside toggle -->
			<div class="nav-btns">
                            <button class="aside-btn"><i class="fa fa-bars"></i></button>
			</div>
			<!-- /aside toggle -->
                        
                    </div>
		</div>
		<!-- /Main Nav -->
                                
                <!-- Aside Nav -->
		<div id="nav-aside">
                                    
                    <!-- nav -->
                    <div class="section-row">
			<ul class="nav-aside-menu">
                            <li><a href="index.php">Inicio</a></li>
                            <li><a href="#" class="navito-close about">Sobre mi</a></li>
                            <li><a href="correo/index.php" class="navito-close contact">Contacto</a></li>
			</ul>
                    </div>
                    <!-- /nav -->

                    <!-- social links -->
                    <div class="section-row">
			<h3>Sigueme</h3>
			<ul class="nav-aside-social">
                            <li><a href="https://www.facebook.com/profile.php?id=100000704533847" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/josluna98/?hl=es-la" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCCkfl-nUB-glLqijBst3vkg/featured" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
			</ul>
                    </div>
                    <!-- /social links -->

                    <!-- aside nav close -->
                    <button class="nav-aside-close"><i class="fa fa-times"></i></button>
                    <!-- /aside nav close -->
		</div>
		<!-- Aside Nav -->
                
            </div>
            <!-- /Nav -->
	</header>
	<!-- /Header -->

	<!-- section -->
	<div class="section">
            <!-- container -->
            <div class="container">
                <!-- block -->
                <div id='block'>
                    <!-- row -->
                    <div id='replace' class="row">
                        <div class="col-md-8">
                            <div id="posts" class="row">
                                <!-- aqui trabaja un JavaScript bakansisimo -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- post widget -->
                            <div id="featured" class="aside-widget">
                                <!-- aqui trabaja un JavaScript bakansisimo -->
                            </div>
                            <!-- post widget close -->

                            <!-- post widget -->
                            <div id="read" class="aside-widget">
                                <!-- aqui trabaja un JavaScript bakansisimo -->
                            </div>
                            <!-- post widget close -->
                        </div>

                    </div>
                    <!-- row close -->
                </div>
                <!-- block close -->
            </div>
            <!-- container close -->
	</div>
	<!-- section close -->

        <!-- Footer -->
		<footer id="footer">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-5">
						<div class="footer-widget">
							<div class="footer-logo">
								<a href="index.html" class="logo"><img src="../img/logo.png" alt=""></a>
							</div>
							<ul class="footer-nav">
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Advertisement</a></li>
							</ul>
							<div class="footer-copyright">
								<span>&copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="row">
							<div class="col-md-6">
								<div class="footer-widget">
									<!-- social links -->
                                                                        <div class="section-row">
                                                                            <h3>Sigueme</h3>
                                                                            <ul class="nav-aside-social">
                                                                                <li><a href="https://www.facebook.com/profile.php?id=100000704533847" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                                                <li><a href="https://www.instagram.com/josluna98/?hl=es-la" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                                                <li><a href="https://www.youtube.com/channel/UCCkfl-nUB-glLqijBst3vkg/featured" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                        <!-- /social links -->
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</footer>
		<!-- /Footer -->
        
	<!-- jQuery Plugins -->
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
        <script src="js/crafty-min.js"></script>
        <script src="js/pixi.min.js"></script>
        <script src="js/phaser.min.js"></script>
	<script src="js/main.js"></script>
        <script src="js/posts.js"></script>
    </body>
</html>
