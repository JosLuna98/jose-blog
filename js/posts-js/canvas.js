            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext("2d");
            ctx.fillStyle = "rgb(250,0,0)";
            ctx.fillRect (205, 0, 200, 125);
            ctx.fillStyle = "rgb(0, 0, 250)";
            ctx.fillRect (0, 125, 205, 125);
            ctx.fillStyle = "rgb(0,0,250)";
            ctx.lineWidth = 3;
            var X = (canvas.width / 4)-1;
            var Y = (canvas.height / 4)-1;
            var R = 35;
            var L = 5;
            var paso = 2;	
            var estrella= L / paso;
            var rad = (2*Math.PI) / estrella;
            ctx.beginPath();
            for( var i = 0; i < L; i++ ){
		x = X + R * Math.cos( rad*i );
		y = Y + R * Math.sin( rad*i );
		ctx.lineTo(x, y);
            }
            ctx.closePath();
            ctx.stroke();
            ctx.fill();
            ctx.fillStyle = "rgb(250,0,0)";
            ctx.lineWidth = 3;
            var X = (canvas.width / 4)+200;
            var Y = (canvas.height / 4)+125;
            var R = 35;
            var L = 5;
            var paso = 2;	
            var estrella= L / paso;
            var rad = (2*Math.PI) / estrella;
            ctx.beginPath();
            for( var i = 0; i < L; i++ ){
		x = X + R * Math.cos( rad*i );
		y = Y + R * Math.sin( rad*i );
		ctx.lineTo(x, y);
            }
            ctx.closePath();
            ctx.stroke();
            ctx.fill();