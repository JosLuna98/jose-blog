var posts = [];

$.getJSON("/json/posts.json", function(json) {
    for(i=0; i<json.posts.length; i++){
        posts[i] = json.posts[i];
    }
    //console.log(posts);
    for(i=posts.length-1; i>=0; i--){
        if(posts[i].id===posts.length-1) post(posts[i], true); else post(posts[i]);
    }
    featured();
    read();
    ready();
});

function post(post, c = false){
    var a = document.createElement('div');
    var b;
    if(c){
        a.className = 'col-md-12';
        b = `<div class='post post-thumb'>
                <a id='${post.id}' class='post-img show-modal' href='#'>
                    <img src='${post.url}'></img>`;
    } else {
        a.className = 'col-md-6';
        a.setAttribute('style','height: 362.65px;');
        b = `<div class='post'>
                <a id='${post.id}' class='post-img show-modal' href='#'>
                    <img class='ir' src='${post.url}'></img>`;
    }
    b += `</a>
                <div class='post-body'>
                    <div class='post-meta'>
                        <a class='post-category category'>
                            ${post.etiqueta}
                        </a>
                        <span class='post-date'>
                            ${post.fecha}
                        </span>
                    </div>
                    <h3 class='post-title'>
                        <a id='${post.id}' class='show-modal' href='#'>
                            ${post.titulo}
                        </a>
                    </h3>
                </div>
            </div>`;
    a.innerHTML = b.trim();
    document.querySelector('#posts').appendChild(a);
}

function featured(){
    var title_div = document.createElement('div');
    title_div.className = 'section-title';
    var title_h2 = document.createElement('h2');
    title_h2.textContent = 'Post Destacados';
    
    title_div.appendChild(title_h2);
    
    document.querySelector('#featured').appendChild(title_div);
    
    const feat = [posts[9],posts[8],posts[6]];
    
    for(i=0; i<feat.length; i++){
        var a = document.createElement('div');
        a.className = 'post post-thumb';
        var b = `<a id='${feat[i].id}' class='post-img show-modal' href='#'>
                    <img class='ir' src='${feat[i].url}'></img>
                </a>
                <div class='post-body'>
                    <div class='post-meta'>
                        <a class='post-category category'>
                            ${feat[i].etiqueta}
                        </a>
                        <span class='post-date'>
                            ${feat[i].fecha}
                        </span>
                    </div>
                    <h3 class='post-title'>
                        <a id='${feat[i].id}' class='show-modal' href='#'>
                            ${feat[i].titulo}
                        </a>
                </div>`;
        a.innerHTML = b.trim();
        document.querySelector('#featured').appendChild(a);
    }
}

function read(){
    var title_div = document.createElement('div');
    title_div.className = 'section-title';
    var title_h2 = document.createElement('h2');
    title_h2.textContent = 'Mas leidos';
    
    title_div.appendChild(title_h2);
    
    document.querySelector('#read').appendChild(title_div);
    
    const red = [posts[1],posts[4],posts[5],posts[13]];
    
    for(i=0; i<red.length; i++){
        var a = document.createElement('div');
        a.className = 'post post-widget';
        var b =`<a id='${red[i].id}' class='post-img show-modal' href='#'>
                <img src='${red[i].url}'></img>
                </a>
                <div class='post-body'>
                    <h3 class='post-title'>
                        <a id='${red[i].id}' class='show-modal' href='#'>
                            ${red[i].titulo}
                        </a>
                    </h3>
                </div>`;
        a.innerHTML = b.trim();
        document.querySelector('#read').appendChild(a);
    }
}

function ready(){
    var links = document.getElementsByClassName('show-modal');
    for (var i = 0; i < links.length; i++) {
        listener(links[i]);
    }
}

function listener(link){
    var titulo = document.createElement('h1');
    titulo.className = 'modal-title';
    titulo.textContent = posts[link.id].titulo;
    var contenido = document.createElement('div');
    contenido.className = 'modal-content';
    contenido = file(contenido, posts[link.id].contenido);
    var block = document.createElement('div');
    block.appendChild(titulo);
    block.appendChild(contenido);
    link.addEventListener('click', () => {
        document.querySelector('#block').replaceChild(block, document.querySelector('#replace'));
        if(posts[link.id].script !== "") newScript(posts[link.id].script);
    });
}

function file(contenido, arch){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        contenido.innerHTML = this.responseText;
    };
    xhttp.open("GET", `/res/${arch}.html`, true);
    xhttp.send();
    return contenido;
}

function newScript(array){
    array.forEach(function(entry) {
        var scrpt = document.createElement('script');
        scrpt.setAttribute('src',`js/posts-js/${entry}.js`);
        document.body.appendChild(scrpt);
    });
}